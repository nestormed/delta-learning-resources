import * as React from 'react';
import { View, TextInput} from 'react-native';
import { BottomNavigation, Text, Appbar } from 'react-native-paper';
import HomeActivity from './components/HomeActivity';
import Profile2Activity from './components/Profile2Activity';
import Niveles from './components/ListadosActivity';
import Navbottom from './components/navbottom';
import { StackNavigator,createSwitchNavigator, createStackNavigator,createAppContainer } from 'react-navigation';
import NavigationService from './NavigationService';
import Reproductor from './components/reproductor';
import Password from './components/password';
import Audios from './components/audios';
import Login from './components/login';
import Registro from './components/registro';

const RootStack = createStackNavigator(
{
  bottom: { screen: Navbottom,   navigationOptions: {
        header: null,
      } },

  Niveles: { screen: Niveles },
  Audios: { screen: Audios },

  Reproductor: { screen: Reproductor },
  Password: { screen: Password },
  Login: { screen: Login, navigationOptions: {
        header: null,
      } },
  Registro: { screen: Registro, navigationOptions: {
        header: null,
      } },


},
{
    initialRouteName: 'Login',
}

);

const AppContainer = createAppContainer(RootStack);

// Now AppContainer is the main component for React to render
class App extends React.Component {


  render() {
    return (
      <AppContainer ref={navigatorRef => {
          NavigationService.setTopLevelNavigator(navigatorRef);
        }}/>

    );
  }


}
export default App;