// NavigationService.js

import { NavigationActions } from 'react-navigation';

let _navigator;

function setTopLevelNavigator(navigatorRef) {
  _navigator = navigatorRef;
  console.log('===========================');
  console.log({_navigator});
}

function navigate(routeName, params) {
  _navigator.dispatch(
    NavigationActions.navigate({
      routeName,
      params,
    })
  );
}
function getParam(param, defaultparam) {
  _navigator.dispatch(
  	getParam({
      param,
      defaultparam,

  	})
    

    
  );
}

// add other navigation functions that you need and export them

export default {
  navigate,
  setTopLevelNavigator,
  getParam,
};