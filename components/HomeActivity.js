import React from 'react';
import { FlatList, StyleSheet, ActivityIndicator, View, Image,Platform,  } from 'react-native';
import { Appbar, DefaultTheme,List, Card, Title, Paragraph, Button, Avatar, Checkbox, Provider as PaperProvider, BottomNavigation, Text } from 'react-native-paper';

import {StackNavigator,createSwitchNavigator, TabNavigator,createStackNavigator, createAppContainer, NavigationActions } from 'react-navigation';
import {NavigationService as hh} from './../NavigationService';
import ListadosActivity from './ListadosActivity';
import Navegador2 from './navegador2';

const RootStack = createStackNavigator(
{


  Listados: { screen: HomeActivity },
}
);
const AppContainer2 = createAppContainer(RootStack);
class HomeActivity extends React.Component {




  constructor(props){
    super(props);
    this.state ={ isLoading: true}
    console.log('WWWWWWWWWWWWWWWWWWWWWWWW-------WWW');
    
    console.log('WWWWWWWWWWWWWWWWWWWWWWWW-------WWW');
  }



  componentDidMount(){
    return fetch('https://facebook.github.io/react-native/movies.json')
      .then((response) => response.json())
      .then((responseJson) => {

        this.setState({
          isLoading: false,
          dataSource: responseJson.movies,
        }, function(){

        });

      })
      .catch((error) =>{
        console.error(error);
      });
  }



  render(){
    console.log(this);

    if(this.state.isLoading){
      return(
        <View style={{flex: 1, padding: 20}}>
          <ActivityIndicator/>
        </View>
      )
    }

    return(


      <View style={{flex:1, marginTop:0}}>

 
        <View style={stilos.header2}>
          
          <View>
            <Image source={ require('../images/logo.png') } style={{width: 50, height: 50}} />
          </View>
        </View>






        <View style={stilos.v_container}>
          <FlatList
            
            data={this.state.dataSource}
            renderItem={({item}) => 

            <View style={{flex: 1, flexDirection: 'row',alignItems:'stretch'}}>
              <View style={stilos.container}>
                <View>
                  <Text  style={{color:'#000', fontSize:22,left:10, top:10}}>Libro</Text>
                  <Text style={{color:'#333', fontSize:16, left:10, top:15,textAlign:'justify'}}>Aquí se encontrará la descripción de los libros</Text>
                </View>
                
                <View>
                  <View>
                    <Button color='#00964a' onPress={() => this.props.navigation.navigate('Listados', {
              itemId: 86,
            });}>Ver Niveles</Button> 
                  </View>
                </View>
                
              </View>
              <View style={stilos.containerFoto}>
               <Image source={{ uri: 'https://facebook.github.io/react/logo-og.png' }} style={{width: 90, height: 137.5}} />
              </View>
            </View>



              
            
          }
            keyExtractor={({id}, index) => id}
          />

        </View>
        
      </View>

    );
  }


}

const stilos = StyleSheet.create(
  {
    ButtonEscuchar:{
      color:'#00964b',
    },
    v_container: {
      flex: 10,
      padding: 10,
      marginLeft:2,
      marginRight:2,
      
      flexDirection: 'column', // main axis
      
      justifyContent: 'center', // main axis
      /*
      alignItems: 'center', // cross axis
      */
      backgroundColor: '#f9f9f9',
    },
    container: {
      marginTop: 14,
      alignItems: "stretch",
      backgroundColor: '#FFF',
      color: '#00f1ad',
      height:140,
      flex:3,
      justifyContent:'space-between',
      borderTopWidth: 2,
      borderTopColor: '#e52427',
      borderBottomWidth: 0.5,
      borderBottomColor: '#DDD',
      /*
      borderRightWidth: 0.5,
      borderRightColor: '#DDD',*/
      borderLeftWidth: 0.5,
      borderLeftColor: '#DDD',


      
      
    },
    containerFoto:{

      flex:2, 
      flexDirection: 'row-reverse', 
      backgroundColor:'#FFF', 
      height: 140,
      marginTop:14,
      
      borderColor: '#d6d7da',
      borderTopWidth: 2,
      borderTopColor: '#e52427',
      borderBottomWidth: 0.5,
      borderBottomColor: '#ddd',
      borderRightWidth: 0.5,
      borderRightColor: '#ddd',
      /*borderLeftWidth: 0.5,
      borderLeftColor: '#ddd',*/


      

    },
    bottom: {
      position: 'absolute',
      left: 0,
      right: 0,
      bottom: 0,
      height:60,
    },
    backBottom:{
      height:60,
    },
    header:{

      flex:1,
      flexDirection:'row',
      justifyContent: 'center',
      alignContent:'center',
      backgroundColor:'#004d9a',

    },
    header2:{

      flex:2,
      flexDirection:'column-reverse',
      
      alignItems:'center',
      backgroundColor:'#00009a',

    }

  });

export default HomeActivity;
