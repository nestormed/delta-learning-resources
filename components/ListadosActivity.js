import React from 'react';
import { FlatList, StyleSheet, ActivityIndicator, View, Image,Platform,  } from 'react-native';
import { Appbar, Divider, TouchableRipple, List, Card, Title, Paragraph, Button, Avatar, Checkbox, Provider as PaperProvider, BottomNavigation, Text } from 'react-native-paper';

import {StackNavigator,navigationOptions} from 'react-navigation';



class Niveles extends React.Component {


static navigationOptions = {
title: 'Niveles  ',
headerStyle: {
backgroundColor: '#00009a',
},
headerTintColor: '#fff',
headerTitleStyle: {
fontWeight: 'bold',
},

};

  constructor(props){
    super(props);
    this.state ={ isLoading: true}
  }





  componentDidMount(){
    const { navigation } = this.props;
    const itemId = navigation.getParam('itemId', 'NO-ID');
    console.log('-----------------------------');
    console.log(this.props.navigation.state.params.itemId);
    return fetch('https://deltalearningapp.com/sections_by_book/'+this.props.navigation.state.params.itemId)
      .then((response) => response.json())
      .then((responseJson) => {

        this.setState({
          isLoading: false,
          dataSource: responseJson.sections,
        }, function(){

        });

      })
      .catch((error) =>{
        console.error(error);
      });
  }




  render(){






    if(this.state.isLoading){
      return(
        <View style={{flex: 1, padding: 20, justifyContent:'center'}}>
          <ActivityIndicator/>
        </View>
      )
    }

    return(


      <View style={{flex:1, marginTop:0}}>






        <View style={stilos.v_container}>
          <FlatList
            
            data={this.state.dataSource}
            renderItem={({item}) => 

            <View style={{flex: 1, flexDirection: 'row',alignItems:'stretch',borderBottomWidth:2, borderBottomColor:'#ddd', paddingTop:10,}}>
              <TouchableRipple style={{flex: 1, flexDirection: 'row',alignItems:'stretch'}}
                onPress={() => this.props.navigation.navigate('Audios',{id_audio:item.id})}
                rippleColor="rgba(0, 0, 0, .32)"
              >
                <Text style={stilos.niveles}>{item.title}</Text>
              </TouchableRipple>
              <Divider />

            </View>



              
            
          }
            keyExtractor={({id}, index) => id.toString()}
          />

        </View>
      </View>
    );
  }


}

const stilos = StyleSheet.create(
  {
    v_container: {
      flex: 10,
      padding: 10,
      marginLeft:2,
      marginRight:2,
      
      flexDirection: 'column', // main axis
      
      justifyContent: 'center', // main axis
      /*
      alignItems: 'center', // cross axis
      */
      backgroundColor: '#f9f9f9',
    },
    container: {
      marginTop: 14,
      alignItems: "stretch",
      backgroundColor: '#FFF',
      color: '#00f1ad',
      height:140,
      flex:3,
      justifyContent:'space-between',
      borderTopWidth: 2,
      borderTopColor: '#ff00f0',
      borderBottomWidth: 0.5,
      borderBottomColor: '#DDD',
      /*
      borderRightWidth: 0.5,
      borderRightColor: '#DDD',*/
      borderLeftWidth: 0.5,
      borderLeftColor: '#DDD',


      
      
    },
    containerFoto:{

      flex:2, 
      flexDirection: 'row-reverse', 
      backgroundColor:'#FFF', 
      height: 140,
      marginTop:14,
      
      borderColor: '#d6d7da',
      borderTopWidth: 2,
      borderTopColor: '#ff00f0',
      borderBottomWidth: 0.5,
      borderBottomColor: '#ddd',
      borderRightWidth: 0.5,
      borderRightColor: '#ddd',
      /*borderLeftWidth: 0.5,
      borderLeftColor: '#ddd',*/


      

    },
    bottom: {
      position: 'absolute',
      left: 0,
      right: 0,
      bottom: 0,
      height:60,
    },
    backBottom:{
      height:60,
    },
    header:{

      flex:1,
      flexDirection:'row',
      alignContent:'stretch',
      backgroundColor:'#0741E7',

    },
    header2:{

      flex:2,
      flexDirection:'column-reverse',
      
      alignItems:'center',
      backgroundColor:'#00009a',

    },
    niveles:{
      fontSize:20,
      color:'#333',
      marginTop:10,
      borderBottomColor:'#ddd',
      borderBottomWidth:0,

    }


  });

export default Niveles;
