import React from 'react';
import { FlatList, StyleSheet, ActivityIndicator, View, Image,Platform,  } from 'react-native';
import { Appbar, DefaultTheme,List, Card, Title, Paragraph, Button, Avatar, Checkbox, Provider as PaperProvider, BottomNavigation, Text } from 'react-native-paper';

import {StackNavigator,} from 'react-navigation';

class ProfileActivity extends React.Component {

static navigationOptions = {
title: 'Profile',
headerStyle: {
backgroundColor: '#03A9F4',
},
headerTintColor: '#fff',
headerTitleStyle: {
fontWeight: 'bold',
},
};







  render(){


    return(
          <View style={{marginTop:30, flex:1, flexDirection:'column',alignItems:'stretch', justifyContent:'center', backgroundColor: '#fff',}}>

            <View style={{flex: 1, flexDirection: 'column',alignItems:'stretch'}}>
              <View style={stilos.containerProfile}>
                <View style={{flex:2, justifyContent:'center', alignItems:'center'}}>
                 <Image source={{ uri: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRhzi4MVbA6VS7TuF12dtoGRwpyEJe9UNnMkAHo7gxte0C-nQBn' }} style={{width: 150, height: 150}} />
                </View>
                <View  style={{flex:3, backgroundColor:'#FFF'}}>
                  <Text  style={{color:'#555', fontSize:16,marginLeft:10, marginTop:0, textAlign:'center'}}>USERNAME: Username</Text>
                  <Text  style={{color:'#555', fontSize:16,marginLeft:10, marginTop:20, textAlign:'center'}}>NOMBRE: Nombre</Text>
                  <Text style={{color:'#555', fontSize:16, marginLeft:10, marginTop:20, textAlign:'center'}}>APELLIDO:Apellido</Text>
                  <Text style={{color:'#555', fontSize:16, marginLeft:10, marginTop:20, textAlign:'center'}}>TELÉFONO:Teléfono</Text>
                  <Text  style={{color:'#555', fontSize:16,marginLeft:10, marginTop:20, textAlign:'center'}}>CORREO: Correo@delta.com</Text>
                </View>  
              </View>

            </View>

          </View>
    );
  }


}

const stilos = StyleSheet.create(
  {
    ButtonEscuchar:{
      color:'#00964b',
    },
    v_container: {
      flex: 10,
      padding: 10,
      marginLeft:2,
      marginRight:2,
      
      flexDirection: 'column', // main axis
      
      justifyContent: 'center', // main axis
      /*
      alignItems: 'center', // cross axis
      */
      backgroundColor: '#f9f9f9',
    },
    container: {
      marginTop: 14,
      alignItems: "stretch",
      backgroundColor: '#FFF',
      color: '#00f1ad',
      height:140,
      flex:3,
      justifyContent:'space-between',
      borderTopWidth: 2,
      borderTopColor: '#e52427',
      borderBottomWidth: 0.5,
      borderBottomColor: '#DDD',
      /*
      borderRightWidth: 0.5,
      borderRightColor: '#DDD',*/
      borderLeftWidth: 0.5,
      borderLeftColor: '#DDD',


      
      
    },
    containerProfile: {
      marginTop: 14,
      alignItems: "stretch",
      backgroundColor: '#FFF',
      color: '#00f1ad',
      height:140,
      flex:3,
      justifyContent:'space-between',
      borderTopWidth: 2,
      borderTopColor: '#e52427',



      
      
    },
    containerFotoProfile:{

      flex:2, 
      flexDirection: 'row-reverse', 
      backgroundColor:'#FFF', 
      height: 140,
      marginTop:14,
      paddingTop:25,
      
      borderColor: '#d6d7da',
      borderTopWidth: 2,
      borderTopColor: '#e52427',
      borderBottomWidth: 0.5,
      borderBottomColor: '#ddd',
      borderRightWidth: 0.5,
      borderRightColor: '#ddd',
      /*borderLeftWidth: 0.5,
      borderLeftColor: '#ddd',*/


      

    },
    containerFoto:{

      flex:2, 
      flexDirection: 'row-reverse', 
      backgroundColor:'#FFF', 
      height: 140,
      marginTop:14,

      
      borderColor: '#d6d7da',
      borderTopWidth: 2,
      borderTopColor: '#e52427',
      borderBottomWidth: 0.5,
      borderBottomColor: '#ddd',
      borderRightWidth: 0.5,
      borderRightColor: '#ddd',
      /*borderLeftWidth: 0.5,
      borderLeftColor: '#ddd',*/


      

    },
    bottom: {
      position: 'absolute',
      left: 0,
      right: 0,
      bottom: 0,
      height:60,
    },
    backBottom:{
      height:60,
    },
    header:{

      flex:1,
      flexDirection:'row',
      alignContent:'stretch',
      backgroundColor:'#004d9a',

    }


  });

export default ProfileActivity;
