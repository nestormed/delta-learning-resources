import * as React from 'react';
import { Appbar } from 'react-native-paper';
import { StyleSheet } from 'react-native';

export default class MyComponent extends React.Component {
  render() {
    return (
      <Appbar style={styles.bottom}>
        <Appbar.Action icon="Book" onPress={() => console.log('Pressed archive')} />
        <Appbar.Action icon="Album" onPress={() => console.log('Pressed mail')} />
        <Appbar.Action icon="History" onPress={() => console.log('Pressed label')} />
        
      </Appbar>
    );
  }
}

const styles = StyleSheet.create({
  bottom: {
    position: 'absolute',
    left: 0,
    right: 0,
    bottom: 0,
  },
});