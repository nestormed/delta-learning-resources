import React from 'react';
import { FlatList, StyleSheet, ActivityIndicator, View, Image,Platform,  } from 'react-native';
import { Appbar, Divider, TouchableRipple, List, Card, Title, Paragraph, Button, Avatar, Checkbox, Provider as PaperProvider, BottomNavigation, Text } from 'react-native-paper';

import {StackNavigator,navigationOptions} from 'react-navigation';


global.listaR=[];
global.indexR=0;
global.idAudio=0;
global.imageLibro='';
class PlaylistItem {
  constructor(name, uri, isVideo) {
    this.name = name;
    this.uri = uri;
    this.isVideo = isVideo;
  }
}
class ListadosActivity extends React.Component {


static navigationOptions = {
title: 'Audios  ',
headerStyle: {
backgroundColor: '#00009a',
},
headerTintColor: '#fff',
headerTitleStyle: {
fontWeight: 'bold',
},

};

  constructor(props){
    super(props);
    this.state ={ isLoading: true}
  }



  componentDidMount(){
    console.log('===========HHHHHHHHH===========');
    console.log(this.props.navigation);
    console.log('-----------------YYYYYYYYYYYY------------');
    console.log(this.props.navigation.state.params.id_audio+' 00000000000000000000');
    return fetch('https://deltalearningapp.com/get_audios_by_section/'+this.props.navigation.state.params.id_audio)
      .then((response) => response.json())
      .then((responseJson) => {

        this.setState({
          isLoading: false,
          dataSource: responseJson.audiobooks,
        }, function(){

          /*console.log(this.state.dataSource);*/

        });

      })
      .catch((error) =>{
        console.error(error);
      });
  }
  armarLista= (item) =>{

    /*console.log('funciono');*/
    global.listaR.push(
      new PlaylistItem(
        item.title,
        "https://deltalearningapp.com/storage/"+item.stored_name,
        false
      ),);
          console.log('la lista global22222222222222 es:');
        console.log(global.listaR[0].uri);
    /*console.log(PLAYLIST);*/
  }
  async prueba(item){

    this.setState({isLoading:true});

    await this.getList(item);


            await console.log('la lista global11 es:');




  }
async  getList(item){


  


     /* console.log('-----------------YYYYYYYYYYYY------------');
      console.log(this.props.navigation.state.params.itemId+' 00000000000000000000');*/
      const response = await fetch('https://deltalearningapp.com/get_section_audios/'+item.id);

        const responseJson = await response.json();
             const a = await this.p(responseJson);

         console.log('################----------##################'+item.id);
         await console.log(responseJson.audiobooks.length);
                   await console.log('dntro de get list function');
           await console.log(responseJson.audiobooks[0]);
           await responseJson.audiobooks.forEach(this.armarLista);
           await console.log(global.listaR[0]);
           await this.asignarIndex(responseJson, item.id);
           await this.props.navigation.navigate('Reproductor',{itemId:item.id});
           







    

  }
  asignarIndex(a, id){

    global.indexR = a.audiobook.order -1;
    global.idAudio = id;
    global.imageLibro = a.icon_book.icon_name;
    console.log('------------------TTTTTTTTT-------------');
    console.log(global.indexR);
    this.setState({isLoading:false});


  }
  p(pp){

    console.log('uuuuuuuuuuuuuuuuu');
  }

  render(){

    const { navigation } = this.props;
    const itemId = navigation.getParam('id_audio', 'NO-ID');
    console.log('TTTTTTTTTTTTTTTTTTTT');
    console.log(itemId+'ioioioioiiioio');


    if(this.state.isLoading){
      return(
        <View style={{flex: 1, padding: 20, justifyContent:'center'}}>
          <ActivityIndicator/>
        </View>
      )
    }

    return(



      <View style={{flex:1, marginTop:0}}>






        <View style={stilos.v_container}>
          <FlatList
            
            data={this.state.dataSource}
            renderItem={({item}) => 

            <View style={{flex: 1, flexDirection: 'row',alignItems:'stretch',borderBottomWidth:2, borderBottomColor:'#ddd', paddingTop:10,}}>
              <TouchableRipple style={{flex: 1, flexDirection: 'row',alignItems:'stretch'}}
                onPress={() => this.prueba(item)}
                rippleColor="rgba(0, 0, 0, .32)"
              >
                <Text style={stilos.niveles}>{item.title}</Text>
              </TouchableRipple>

              <Divider />

            </View>



              
            
          }
            keyExtractor={({id}, index) => id.toString()}
          />

        </View>
      </View>
    );
  }


}

const stilos = StyleSheet.create(
  {
    v_container: {
      flex: 10,
      padding: 10,
      marginLeft:2,
      marginRight:2,
      
      flexDirection: 'column', // main axis
      
      justifyContent: 'center', // main axis
      /*
      alignItems: 'center', // cross axis
      */
      backgroundColor: '#f9f9f9',
    },
    container: {
      marginTop: 14,
      alignItems: "stretch",
      backgroundColor: '#FFF',
      color: '#00f1ad',
      height:140,
      flex:3,
      justifyContent:'space-between',
      borderTopWidth: 2,
      borderTopColor: '#ff00f0',
      borderBottomWidth: 0.5,
      borderBottomColor: '#DDD',
      /*
      borderRightWidth: 0.5,
      borderRightColor: '#DDD',*/
      borderLeftWidth: 0.5,
      borderLeftColor: '#DDD',


      
      
    },
    containerFoto:{

      flex:2, 
      flexDirection: 'row-reverse', 
      backgroundColor:'#FFF', 
      height: 140,
      marginTop:14,
      
      borderColor: '#d6d7da',
      borderTopWidth: 2,
      borderTopColor: '#ff00f0',
      borderBottomWidth: 0.5,
      borderBottomColor: '#ddd',
      borderRightWidth: 0.5,
      borderRightColor: '#ddd',
      /*borderLeftWidth: 0.5,
      borderLeftColor: '#ddd',*/


      

    },
    bottom: {
      position: 'absolute',
      left: 0,
      right: 0,
      bottom: 0,
      height:60,
    },
    backBottom:{
      height:60,
    },
    header:{

      flex:1,
      flexDirection:'row',
      alignContent:'stretch',
      backgroundColor:'#0741E7',

    },
    header2:{

      flex:2,
      flexDirection:'column-reverse',
      
      alignItems:'center',
      backgroundColor:'#00009a',

    },
    niveles:{
      fontSize:20,
      color:'#333',
      marginTop:10,
      borderBottomColor:'#ddd',
      borderBottomWidth:0,

    }


  });

export default ListadosActivity;
