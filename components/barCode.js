import * as React from 'react';
import { Text, View, StyleSheet, Button, Image, Linking, Dimensions  } from 'react-native';
import Constants from 'expo-constants';
import * as Permissions from 'expo-permissions';

import { BarCodeScanner } from 'expo-barcode-scanner';

export default class Barcode extends React.Component {
  state = {
    hasCameraPermission: null,
    scanned: false,
  };

  async componentDidMount() {
    this.getPermissionsAsync();
  }

  getPermissionsAsync = async () => {
    const { status } = await Permissions.askAsync(Permissions.CAMERA);
    this.setState({ hasCameraPermission: status === 'granted' });
  }

  render() {
    const { hasCameraPermission, scanned } = this.state;

    if (hasCameraPermission === null) {
      return <Text>Requesting for camera permission</Text>;
    }
    if (hasCameraPermission === false) {
      return <Text>No se puede acceder a la camara</Text>;
    }
    return (
      <View
        style={{
          flex: 1,
          flexDirection: 'column',
          justifyContent: 'flex-end',
        }}>
        <View style={stilos.header2}>
          

        </View>
        <BarCodeScanner
          onBarCodeScanned={scanned ? undefined : this.handleBarCodeScanned}
          style={stilos.v_container}
        />

        {scanned && (
          <Button
            title={'Presione para scanear nuevamente'}
            onPress={() => this.setState({ scanned: false })}
          />
        )}
      </View>
    );
  }

  handleBarCodeScanned = ({ type, data }) => {
    this.setState({ scanned: true });
    
    console.log(data.includes('http'));

    if(data.includes('http')){
      Linking.openURL(data);
    }else{

      alert('Codigo no contiene link web ');
    }
    
    
    
  };
}
const { width: DEVICE_WIDTH, height: DEVICE_HEIGHT } = Dimensions.get("window");
const stilos = StyleSheet.create(
  {
    ButtonEscuchar:{
      color:'#00964b',
    },
    v_container: {
      flex: 10,
      width:DEVICE_WIDTH,
      alignItems: "stretch",

      
      flexDirection: 'column', // main axis
      
      justifyContent: 'center', // main axis
      /*
      alignItems: 'center', // cross axis
      */
      backgroundColor: '#f9f9f9',
    },
    container: {
      marginTop: 14,
      alignItems: "stretch",
      backgroundColor: '#FFF',
      color: '#00f1ad',
      height:140,
      flex:3,
      justifyContent:'space-between',
      borderTopWidth: 2,
      borderTopColor: '#e52427',
      borderBottomWidth: 0.5,
      borderBottomColor: '#DDD',
      /*
      borderRightWidth: 0.5,
      borderRightColor: '#DDD',*/
      borderLeftWidth: 0.5,
      borderLeftColor: '#DDD',


      
      
    },
    containerFoto:{

      flex:2, 
      flexDirection: 'row-reverse', 
      backgroundColor:'#FFF', 
      height: 140,
      marginTop:14,
      
      borderColor: '#d6d7da',
      borderTopWidth: 2,
      borderTopColor: '#e52427',
      borderBottomWidth: 0.5,
      borderBottomColor: '#ddd',
      borderRightWidth: 0.5,
      borderRightColor: '#ddd',
      /*borderLeftWidth: 0.5,
      borderLeftColor: '#ddd',*/


      

    },
    bottom: {
      position: 'absolute',
      left: 0,
      right: 0,
      bottom: 0,
      height:60,
    },
    backBottom:{
      height:60,
    },
    header:{

      flex:1,
      flexDirection:'row',
      justifyContent: 'center',
      alignContent:'center',
      backgroundColor:'#004d9a',

    },
    header2:{

      flex:2,
      flexDirection:'column-reverse',
      
      alignItems:'center',
      backgroundColor:'#00009a',

    },
    containerProfile: {
      marginTop: 14,
      alignItems: "stretch",
      backgroundColor: '#FFF',
      color: '#00f1ad',
      height:140,
      flex:3,
      justifyContent:'space-between',
      borderTopWidth: 2,
      borderTopColor: '#e52427',



      
      
    },
    containerFotoProfile:{

      flex:2, 
      flexDirection: 'row-reverse', 
      backgroundColor:'#FFF', 
      height: 140,
      marginTop:14,
      paddingTop:25,
      
      borderColor: '#d6d7da',
      borderTopWidth: 2,
      borderTopColor: '#e52427',
      borderBottomWidth: 0.5,
      borderBottomColor: '#ddd',
      borderRightWidth: 0.5,
      borderRightColor: '#ddd',
      /*borderLeftWidth: 0.5,
      borderLeftColor: '#ddd',*/


      

    },
    containerFoto:{

      flex:2, 
      flexDirection: 'row-reverse', 
      backgroundColor:'#FFF', 
      height: 140,
      marginTop:14,

      
      borderColor: '#d6d7da',
      borderTopWidth: 2,
      borderTopColor: '#e52427',
      borderBottomWidth: 0.5,
      borderBottomColor: '#ddd',
      borderRightWidth: 0.5,
      borderRightColor: '#ddd',
      /*borderLeftWidth: 0.5,
      borderLeftColor: '#ddd',*/


      

    },
    bottom: {
      position: 'absolute',
      left: 0,
      right: 0,
      bottom: 0,
      height:60,
    },
    backBottom:{
      height:60,
    },
    header:{

      flex:1,
      flexDirection:'row',
      alignContent:'stretch',
      backgroundColor:'#004d9a',

    }

  });