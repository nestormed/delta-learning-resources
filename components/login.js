import React from 'react';
import { FlatList, StyleSheet, ActivityIndicator, View, Image,Platform, TextInput  } from 'react-native';
import { Appbar, Dialog, Divider, TouchableRipple, List, Card, Title, Paragraph, Button, Avatar, Checkbox, Provider as PaperProvider, BottomNavigation, Text } from 'react-native-paper';

import {StackNavigator,navigationOptions} from 'react-navigation';

const control={c:0};
class Login extends React.Component {



	constructor(props) {
	   super(props);
	   global.datosUser = {email:''};
	   global.controlList = {control:0};
	   this.state = {olvide:'',nombre: '', email: '', contraseña:'', contraseñaR:'', visible:false, mensaje:'', isLoading: false, login:false};
	 }

	 componentWillMount() {

	 	console.log('gasdghfjhsdgk');

	 }
	 _showDialog = () => this.setState({ visible: true });

	  _hideDialog = () => this.setState({ visible: false });

	  _enviarPassword = () => this.props.navigation.navigate('Password');

	 aceptaLogin= () => {

	 	

	 		
	 			if(true){

	 				console.log('mensaje es:')
	 				console.log(this.state.dataSource);

	 				if (this.state.dataSource =='0') {

	 					global.datosUser.email=this.state.email;
	 					this.props.navigation.navigate('bottom');
	 					return;
	 				}
	 				if(this.state.dataSource =='1'){

	 					this.setState({ mensaje:'Usuario o email no registrado', login:false })
	 					this._showDialog();
	 					return;
	 				}
	 				if(this.state.dataSource =='2'){

	 					this.setState({ mensaje:'Contraseña incorrecta', login:false, olvide:'Olvide contraseña' })
	 					this._showDialog();
	 					return;
	 				}else{
	 					if(control.c==0){

	 						control.c=1;
	 						console.log('es cero');
	 						this._loguear();
	 						
	 					}else{

	 						this.setState({ mensaje:'Ha ocurrido un error, contáctenos o intente luego.', login:false })
	 						this._showDialog();
	 						console.log(control.c);
	 						console.log('es uno');
	 						return;

	 					}


	 				}
	 			}
	 		
	 }
	_loguear = () => {
		
	  console.log(`ON LOAD START`);

	  if(this.state.email=='' || this.state.email==null) {

	  	this.setState({ mensaje:'Coloque email' })
	  	this._showDialog();
	  	return;
	  }
	  if(this.state.contraseña=='' || this.state.contraseña==null) {

	  	this.setState({ mensaje:'Coloque contraseña' })
	  	this._showDialog();
	  	return;
	  }else{
	  	console.log('*******TTTTTTTTTT***********');
	  	this.setState({login:true});

	  	this.setState({ isLoading: true });

	  	fetch('https://deltalearningapp.com/login_app'
	  		, {
	  		  method: 'POST',
	  		  headers: {
	  		    Accept: 'application/json',
	  		    'Content-Type': 'application/json',
	  		  },
	  		  body: JSON.stringify({
	  		    email: this.state.email,
	  		    password: this.state.contraseña,
	  		  }),
	  		})
	  	.then((response) => response.json())
	  	.then((responseJson) => {

	  	  this.setState({
	  	    isLoading: false,
	  	    dataSource: responseJson.msg,
	  	  }, function(){

	  	  	console.log('dentro del callback del json');
	  	  	console.log(this.state.dataSource);
	  	  	if(this.state.login){

	  	  		this.aceptaLogin();

	  	  	}


	  	  });

	  	})
	  	.catch((error) =>{
	  	  console.error(error);
	  	  this.setState({ mensaje:'Ha ocurrido un error intente de nuevo' })
	  	  this._showDialog();
	  	  return
	  	});


	  }

	};





	render(){



		if(this.state.isLoading){
		  return(
        <View style={{flex: 1, padding: 20, justifyContent:'center'}}>
          <ActivityIndicator/>
        </View>
		  )
		}



			return(


			<View style={{flex:1, backgroundColor:'#fff'}}>
				<View style={stilos.header2}>
				  
				  <View>
				    <Image source={ require('../images/logo.png') } style={{width: 120, height: 120}} />
				  </View>
				</View>

				<View style={{flex:3, justifyContent:'center', alignItems: "center", alignSelf:'center', flexDirection:'column'}}>
				  
				  	<Text style={{padding: 10, fontSize: 18, color:'#000094'}}>
				  	 	Usuario
				  	</Text>
				    <TextInput
				      style={{height: 40, backgroundColor:'#f5f5f5', width:180}}
				      placeholder="Usuario"
				      onChangeText={(email) => this.setState({email})}
				      value={this.state.email}
				    />

				  
				  
				  	<Text style={{padding: 10, fontSize: 18, color:'#000094', marginTop:3}}>
				  	  Password
				  	</Text>
				    <TextInput
				      secureTextEntry={true}
				      style={{height: 40, backgroundColor:'#f5f5f5', width:180, marginBottom:30}}
				      placeholder="Password"
				      onChangeText={(contraseña) => this.setState({contraseña})}
				      value={this.state.contraseña}
				    />


				    <View style={{flexDirection:'column', justifyContent:'space-around', flex:1}}>
					     <Button color='#000094' mode="contained" onPress={() => this._loguear()}>
					      Entrar
					    </Button>
					     <Button color='#000094' mode="contained" onPress={() => this.props.navigation.navigate('Registro')}>
					      Registrar
					    </Button>
				    </View>

				  
				

				</View>
				<Dialog
				   visible={this.state.visible}
				   onDismiss={this._hideDialog}>
				  <Dialog.Title>Alerta</Dialog.Title>
				  <Dialog.Content>
				    <Paragraph>{this.state.mensaje}</Paragraph>
				  </Dialog.Content>
				  <Dialog.Actions>
				  	<Button onPress={this._enviarPassword}>{this.state.olvide}</Button>
				    <Button onPress={this._hideDialog}>Ok</Button>
				  </Dialog.Actions>
				</Dialog>
			</View>
			);


	}



	}

const stilos = StyleSheet.create(
  {


  	header2:{

  	  flex:2,
  	  
  	  justifyContent:'center',
  	  
  	  alignItems:'center',
  	  backgroundColor:'#00009a',

  	}
  }	);
export default Login;