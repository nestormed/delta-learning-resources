import * as React from 'react';


import {Dialog, DefaultTheme,List, Card, Title, Paragraph, Button, Avatar, Checkbox, Provider as PaperProvider, BottomNavigation, Text } from 'react-native-paper';


import { StackNavigator,createSwitchNavigator, createStackNavigator,createAppContainer } from 'react-navigation';



import { FlatList, StyleSheet, ActivityIndicator, View, Image,Platform, TextInput  } from 'react-native';

import NavigationService from '.././NavigationService';
import Barcode from './barCode'

class ProfileActivity extends React.Component {

static navigationOptions = {
title: 'Profile',
headerStyle: {
backgroundColor: '#03A9F4',
visible:false,
},
headerTintColor: '#fff',
headerTitleStyle: {
fontWeight: 'bold',
},
};

constructor(props) {
   super(props);

   this.state = {isLoading:false, password:'', mensaje:'', dataSource:''};
 }

 _showDialog = () => this.setState({ visible: true });

  _hideDialog = () => this.setState({ visible: false });


componentDidMount(){
  this.cargarDatos();
}

cargarDatos(){

        console.log('*******TTTTTTTTTT***********');
        

        this.setState({ isLoading: true });

        fetch('https://deltalearningapp.com/user_name/'+global.datosUser.email)
        .then((response) => response.json())
        .then((responseJson) => {

          this.setState({
            isLoading: false,
            dataSource: responseJson.user,
          }, function(){

            console.log('dentro del callback del json');
            console.log(this.state.dataSource);



          });

        })
        .catch((error) =>{
          console.error(error);
          this.setState({ mensaje:'Ha ocurrido un error intente de nuevo' })
          this._showDialog();
          return
        });


      
}

cambiarPassword(){

  console.log('*******TTTTTTTTTT***********');
  this.setState({login:true});

  this.setState({ isLoading: true });

  fetch('https://deltalearningapp.com/change_pass'
    , {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        email: global.datosUser.email,
        password: this.state.password,
      }),
    })
  .then((response) => response.json())
  .then((responseJson) => {

    this.setState({
      isLoading: false,
      dataSource: responseJson.msg,
    }, function(){

      console.log('dentro del callback del json');
      console.log(this.state.dataSource);
      if(this.state.dataSource==0){


        this.setState({ mensaje:'Se ha cambiado su password' })
        this._showDialog();
       

      }
      else{

        this.setState({ mensaje:'Ha ocurrido un error intente de nuevo' })
        this._showDialog();
      }


    });

  })
  .catch((error) =>{
    console.error(error);
    this.setState({ mensaje:'Ha ocurrido un error intente de nuevo' })
    this._showDialog();
    return
  });

}






  render(){

    if(this.state.isLoading){
      return(
        <View style={{flex: 1, padding: 20, justifyContent:'center'}}>
          <ActivityIndicator/>
        </View>
      )
    }


    return(
          <View style={{marginTop:30, flex:1, flexDirection:'column',alignItems:'stretch', justifyContent:'center', backgroundColor: '#fff',}}>

            <View style={{flex: 1, flexDirection: 'column',alignItems:'stretch'}}>
              <View style={stilos.containerProfile}>
                <View style={{flex:2, justifyContent:'center', alignItems:'center'}}>
                 <Image source={require('.././images/usuario.png') } style={{width: 150, height: 150}} />
                </View>
                <View  style={{flex:3, backgroundColor:'#FFF'}}>
                  <Text  style={{color:'#555', fontSize:16,marginLeft:10, marginTop:0, textAlign:'center'}}>USERNAME: {global.datosUser.email}</Text>
                  <Text  style={{color:'#555', fontSize:16,marginLeft:10, marginTop:20, textAlign:'center'}}>NOMBRE: {this.state.dataSource.name}</Text>
                  <Text  style={{color:'#555', fontSize:16,marginLeft:10, marginTop:20, textAlign:'center'}}>Si desea cambiar su contraseña, ingrese la nueva abajo.</Text>
                  <View style={{justifyContent:'center', alignItems: "center", alignSelf:'center', flexDirection:'column', flex:1}}>
                  <TextInput
                  secureTextEntry={true}
                    style={{height: 40, backgroundColor:'#f5f5f5', width:180, marginBottom:10}}
                    placeholder="Nueva contraseña"
                    onChangeText={(password) => this.setState({password})}
                    value={this.state.password}
                  />
                   <Button color='#000094' mode="contained" onPress={() => this.cambiarPassword()}>
                    Cambiar
                  </Button>
                  </View>
                </View>  
              </View>

            </View>
            <Dialog
               visible={this.state.visible}
               onDismiss={this._hideDialog}>
              <Dialog.Title>Alerta</Dialog.Title>
              <Dialog.Content>
                <Paragraph>{this.state.mensaje}</Paragraph>
              </Dialog.Content>
              <Dialog.Actions>
                <Button onPress={this._enviarPassword}>{this.state.olvide}</Button>
                <Button onPress={this._hideDialog}>Ok</Button>
              </Dialog.Actions>
            </Dialog>

          </View>
    );
  }


}
class HomeActivity extends React.Component {




  constructor(props){
    super(props);
    this.state ={ isLoading: true}
    console.log('WWWWWWWWWWWWWWWWWWWWWWWW-------WWW');
    
    console.log('WWWWWWWWWWWWWWWWWWWWWWWW-------WWW');
  }



  componentDidMount(){
    return fetch('https://deltalearningapp.com/get_books')
      .then((response) => response.json())
      .then((responseJson) => {

        this.setState({
          isLoading: false,
          dataSource: responseJson.books,
        }, function(){

        });

      })
      .catch((error) =>{
        console.error(error);
      });
  }



  render(){
    /*
    console.log(this);
    console.log('dentro de home3333333333333333333333');
    console.log(NavigationService.getParam());
    */
    
/*
    const { navigation } = NavigationService;
    const itemId = NavigationService.getParam('itemId', 'NO-ID');
    console.log('##########-------------###############');
    console.log(itemId)
    */


    if(this.state.isLoading){
      return(
        <View style={{flex: 1, padding: 20, justifyContent:'center'}}>
          <ActivityIndicator/>
        </View>
      )
    }

    return(


      <View style={{flex:1, marginTop:0}}>

 
        <View style={stilos.header2}>
          

        </View>






        <View style={stilos.v_container}>
          <FlatList
            
            data={this.state.dataSource}
            
            renderItem={({item}) => 

            <View style={{flex: 1, flexDirection: 'row',alignItems:'stretch'}}>
              <View style={stilos.container}>
                <View>
                  <Text  style={{color:'#000', fontSize:22,left:10, top:10}}>{item.title}</Text>
                  <Text style={{color:'#333', fontSize:16, left:10, top:15,textAlign:'justify'}}>{item.description}</Text>
                </View>
                
                <View>
                  <View>
                    <Button color='#00964a' onPress={() => NavigationService.navigate('Niveles',{itemId:item.id})}>Ver Niveles</Button> 
                  </View>
                </View>
                
              </View>
              <View style={stilos.containerFoto}>
               <Image source={{ uri: 'https://deltalearningapp.com/storage/'+item.icon_name }} style={{width: 90, height: 137.5}} />
              </View>
            </View>



              
            
          }
            keyExtractor={({id}, index) => id.toString()}
          />

        </View>
        
      </View>

    );
  }


}


//const cpp = createAppContainer(RootStack);
  
export default class Navbottom extends React.Component {
  state = {
    index: 0,
    routes: [
      { key: 'music', title: 'Libros', icon: 'book' },
      { key: 'barCode', title: 'QR', icon: 'crop-free' },
      { key: 'recents', title: 'Perfil', icon: 'person' },
      


    ],
  };


  _handleIndexChange = index => this.setState({ index });

  _renderScene = BottomNavigation.SceneMap({
    music: HomeActivity,

    recents: ProfileActivity,
    barCode: Barcode,
 
  });



  render(props) {
    console.log('888888888888----------------88888888888888888');
    console.log(this.props);
    return (


      


      <BottomNavigation barStyle = {{backgroundColor:'#00009a'}}
        navigationState={this.state}
        onIndexChange={this._handleIndexChange}
        renderScene={this._renderScene}

        

        
      />
      
    );
  }
}

const stilos = StyleSheet.create(
  {
    ButtonEscuchar:{
      color:'#00964b',
    },
    v_container: {
      flex: 10,
      padding: 10,
      marginLeft:2,
      marginRight:2,
      
      flexDirection: 'column', // main axis
      
      justifyContent: 'center', // main axis
      /*
      alignItems: 'center', // cross axis
      */
      backgroundColor: '#f9f9f9',
    },
    container: {
      marginTop: 14,
      alignItems: "stretch",
      backgroundColor: '#FFF',
      color: '#00f1ad',
      height:140,
      flex:3,
      justifyContent:'space-between',
      borderTopWidth: 2,
      borderTopColor: '#e52427',
      borderBottomWidth: 0.5,
      borderBottomColor: '#DDD',
      /*
      borderRightWidth: 0.5,
      borderRightColor: '#DDD',*/
      borderLeftWidth: 0.5,
      borderLeftColor: '#DDD',


      
      
    },
    containerFoto:{

      flex:2, 
      flexDirection: 'row-reverse', 
      backgroundColor:'#FFF', 
      height: 140,
      marginTop:14,
      
      borderColor: '#d6d7da',
      borderTopWidth: 2,
      borderTopColor: '#e52427',
      borderBottomWidth: 0.5,
      borderBottomColor: '#ddd',
      borderRightWidth: 0.5,
      borderRightColor: '#ddd',
      /*borderLeftWidth: 0.5,
      borderLeftColor: '#ddd',*/


      

    },
    bottom: {
      position: 'absolute',
      left: 0,
      right: 0,
      bottom: 0,
      height:60,
    },
    backBottom:{
      height:60,
    },
    header:{

      flex:1,
      flexDirection:'row',
      justifyContent: 'center',
      alignContent:'center',
      backgroundColor:'#004d9a',

    },
    header2:{

      flex:2,
      flexDirection:'column-reverse',
      
      alignItems:'center',
      backgroundColor:'#00009a',

    },
    containerProfile: {
      marginTop: 14,
      alignItems: "stretch",
      backgroundColor: '#FFF',
      color: '#00f1ad',
      height:140,
      flex:3,
      justifyContent:'space-between',
      borderTopWidth: 2,
      borderTopColor: '#e52427',



      
      
    },
    containerFotoProfile:{

      flex:2, 
      flexDirection: 'row-reverse', 
      backgroundColor:'#FFF', 
      height: 140,
      marginTop:14,
      paddingTop:25,
      
      borderColor: '#d6d7da',
      borderTopWidth: 2,
      borderTopColor: '#e52427',
      borderBottomWidth: 0.5,
      borderBottomColor: '#ddd',
      borderRightWidth: 0.5,
      borderRightColor: '#ddd',
      /*borderLeftWidth: 0.5,
      borderLeftColor: '#ddd',*/


      

    },
    containerFoto:{

      flex:2, 
      flexDirection: 'row-reverse', 
      backgroundColor:'#FFF', 
      height: 140,
      marginTop:14,

      
      borderColor: '#d6d7da',
      borderTopWidth: 2,
      borderTopColor: '#e52427',
      borderBottomWidth: 0.5,
      borderBottomColor: '#ddd',
      borderRightWidth: 0.5,
      borderRightColor: '#ddd',
      /*borderLeftWidth: 0.5,
      borderLeftColor: '#ddd',*/


      

    },
    bottom: {
      position: 'absolute',
      left: 0,
      right: 0,
      bottom: 0,
      height:60,
    },
    backBottom:{
      height:60,
    },
    header:{

      flex:1,
      flexDirection:'row',
      alignContent:'stretch',
      backgroundColor:'#004d9a',

    }

  });


