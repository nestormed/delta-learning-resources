import * as React from 'react';
import { View} from 'react-native';
import { BottomNavigation, Text, Appbar } from 'react-native-paper';

import ListadosActivity from './ListadosActivity';
import HomeActivity from './HomeActivity';

import { StackNavigator,createSwitchNavigator, createStackNavigator,createAppContainer } from 'react-navigation';


const RootStack = createStackNavigator(
{

  Listados: { screen: ListadosActivity },

});


const navegador2 = createAppContainer(RootStack);

// Now AppContainer is the main component for React to render

