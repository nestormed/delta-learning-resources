import React from 'react';
import { FlatList, StyleSheet, ActivityIndicator, View, Image,Platform, TextInput  } from 'react-native';
import { Appbar, Dialog, Divider, TouchableRipple, List, Card, Title, Paragraph, Button, Avatar, Checkbox, Provider as PaperProvider, BottomNavigation, Text } from 'react-native-paper';

import {StackNavigator,navigationOptions} from 'react-navigation';

const control={c:0};
class Login extends React.Component {

	static navigationOptions = {
	
	headerStyle: {
	backgroundColor: '#00009a',
	},
	headerTintColor: '#fff',
	headerTitleStyle: {
	fontWeight: 'bold',
	},

	};


	constructor(props) {
	   super(props);
	   this.state = {olvide:'',nombre: '', email: '', contraseña:'', contraseñaR:'', visible:false, mensaje:'', isLoading: false, login:false};
	 }

	 componentWillMount() {

	 	console.log('gasdghfjhsdgk');

	 }
	 _showDialog = () => this.setState({ visible: true });

	  _hideDialog = () => this.setState({ visible: false });

	  _enviarPass = () => {

	  	console.log('*******TTTTTTTTTT***********');
	  	this.setState({login:true});

	  	this.setState({ isLoading: true });

	  	fetch('https://deltalearningapp.com/enviarPassword/'+this.state.email
	  	)
	  	.then((response) => response.json())
	  	.then((responseJson) => {

	  	  this.setState({
	  	    isLoading: false,
	  	    dataSource: responseJson.msg,
	  	  }, function(){

	  	  	console.log('dentro del callback del json');
	  	  	console.log(this.state.msg);
	  	  	if(this.state.msg){

	  	  		this.setState({ mensaje:'Ha ocurrido un error intente de nuevo' })
	  	  		this._showDialog();

	  	  	}else{

	  	  		this.setState({ mensaje:'Se envió su nueva contraseña a su email' })
	  	  		this._showDialog();
	  	  	}


	  	  });

	  	})
	  	.catch((error) =>{
	  	  console.error(error);
	  	  this.setState({ mensaje:'Ha ocurrido un error intente de nuevo' })
	  	  this._showDialog();
	  	  return
	  	});

	  }


	render(){



		if(this.state.isLoading){
		  return(
		    <View style={{flex: 1, padding: 20}}>
		      <ActivityIndicator/>
		    </View>
		  )
		}



			return(


			<View style={{flex:1, backgroundColor:'#fff'}}>
				<View style={stilos.header2}>
				  
				  <View>
				    <Image source={ require('../images/logo.png') } style={{width: 120, height: 120}} />
				  </View>
				</View>

				<View style={{flex:3, justifyContent:'center', alignItems: "center", alignSelf:'center', flexDirection:'column'}}>
				  
				  	<Text style={{padding: 10, fontSize: 18, color:'#000094'}}>
				  	 	Coloque su email, enviaremos su nueva contraseña
				  	</Text>
				    <TextInput
				      style={{height: 40, backgroundColor:'#f5f5f5', width:180}}
				      placeholder="Usuario"
				      onChangeText={(email) => this.setState({email})}
				      value={this.state.email}
				    />

				  



				    <View style={{flexDirection:'column', justifyContent:'space-around', flex:1}}>
					     <Button color='#000094' mode="contained" onPress={() => this._enviarPass()}>
					      Enviar contraseña
					    </Button>

				    </View>

				  
				

				</View>
				<Dialog
				   visible={this.state.visible}
				   onDismiss={this._hideDialog}>
				  <Dialog.Title>Alerta</Dialog.Title>
				  <Dialog.Content>
				    <Paragraph>{this.state.mensaje}</Paragraph>
				  </Dialog.Content>
				  <Dialog.Actions>
				  	<Button onPress={this._enviarPassword}>{this.state.olvide}</Button>
				    <Button onPress={this._hideDialog}>Ok</Button>
				  </Dialog.Actions>
				</Dialog>
			</View>
			);


	}



	}

const stilos = StyleSheet.create(
  {


  	header2:{

  	  flex:2,
  	  
  	  justifyContent:'center',
  	  
  	  alignItems:'center',
  	  backgroundColor:'#00009a',

  	}
  }	);
export default Login;