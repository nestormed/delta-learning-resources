import React from 'react';
import { FlatList, StyleSheet, ActivityIndicator, View, Image,Platform, TextInput  } from 'react-native';
import { Appbar, Dialog , Divider, TouchableRipple, List, Card, Title, Paragraph, Button, Avatar, Checkbox, Provider as PaperProvider, BottomNavigation, Text } from 'react-native-paper';

import {StackNavigator,navigationOptions} from 'react-navigation';


class Registro extends React.Component {
	constructor(props) {
	   super(props);
	   this.state = {control:false,nombre: '', email: '', contraseña:'', contraseñaR:'', visible:false, mensaje:'', isLoading: false};
	 }
	 _showDialog = () => this.setState({ visible: true });

	  _hideDialog = () => this.setState({ visible: false });

	  _controlarDialog=()=>{

	  	if (this.state.control) {

	  		this.props.navigation.navigate('Login');
	  	}else{
	  		this._hideDialog(); 
	  	};
	  }

	_registrar = () => {
	  console.log(`ON LOAD START`);
	  if(this.state.nombre=='' || this.state.nombre==null) {

	  	
	  	this.setState({ mensaje:'Coloque Nombre' })
	  	this._showDialog();
	  	return;
	  }
	  if(this.state.email=='' || this.state.email==null) {

	  	this.setState({ mensaje:'Coloque email' })
	  	this._showDialog();
	  	return;
	  }
	  if(this.state.contraseña=='' || this.state.contraseña==null) {

	  	this.setState({ mensaje:'Coloque contraseña' })
	  	this._showDialog();
	  	return;
	  }
	  if(this.state.contraseñaR=='' || this.state.contraseñaR==null) {

	  	
	  	this.setState({ mensaje:'Repita contraseña' })
	  	this._showDialog();
	  	return;
	  }
	  if(this.state.contraseña!=this.state.contraseñaR) {

	  	
	  	this.setState({ mensaje:'Contraseñas no coinciden' })
	  	this._showDialog();
	  	return;
	  }else{


	  	this.setState({ isLoading: true });

	  	fetch('https://deltalearningapp.com/register_by_app'
	  		, {
	  		  method: 'POST',
	  		  headers: {
	  		    Accept: 'application/json',
	  		    'Content-Type': 'application/json',
	  		  },
	  		  body: JSON.stringify({
	  		  	name: this.state.nombre,
	  		    email: this.state.email,
	  		    password: this.state.contraseña,
	  		  }),
	  		})
	  	.then((response) => response.json())
	  	.then((responseJson) => {

	  	  this.setState({
	  	    isLoading: false,
	  	    dataSource: responseJson.msg,
	  	  }, function(){

	  	  	  	console.log('mensaje es:')
	  	  	  	console.log(this.state.dataSource);

	  	  	  	if (this.state.dataSource =='0') {

	  	  	  		this.setState({control:true});
	  	  	  		this.setState({ mensaje:'Usted fue registrado exitosamente.' })
	  	  	  		this._showDialog();

	  	  	  		
	  	  	  		
	  	  	  		return;
	  	  	  	}
	  	  	  	if(this.state.dataSource =='1'){

	  	  	  		this.setState({ mensaje:'Correo ya está registrado' })
	  	  	  		this._showDialog();
	  	  	  		return;
	  	  	  	}
	  	  		else{

	  	  	  		this.setState({ mensaje:'Ha ocurrido un error, contáctenos o intente luego.' })
	  	  	  		this._showDialog();
	  	  	  		return;
	  	  	  	}

	  	  });

	  	})
	  	.catch((error) =>{
	  	  console.error(error);
	  	  this.setState({ mensaje:'Ha ocurrido un error, intente de nuevo.' })
	  	  this._showDialog();
	  	});




	  }

	};

	render(){

		if(this.state.isLoading){
		  return(
	        <View style={{flex: 1, padding: 20, justifyContent:'center'}}>
	          <ActivityIndicator/>
	        </View>
		  )
		}

		return(


			<View style={{flex:1, backgroundColor:'#fff'}}>

				<View style={stilos.header2}>
				  
				  <View>
				    <Image source={ require('../images/logo3.png') } style={{width: 110, height: 110}} />
				  </View>
				</View>

				<View style={{flex:8, justifyContent:'center', alignItems: "center", alignSelf:'center', flexDirection:'column'}}>
				  
				  	<Text style={{padding: 10, fontSize: 18, color:'#000094'}}>
				  	 	Nombre completo
				  	</Text>
				    <TextInput
				      style={{height: 40, backgroundColor:'#f5f5f5', width:180}}
				      placeholder="Nombre Apellido"
				      onChangeText={(nombre) => this.setState({nombre})}
				      value={this.state.nombre}
				    />

				  	<Text style={{padding: 10, fontSize: 18, color:'#000094', marginTop:3}}>
				  	  Email
				  	</Text>
				    <TextInput
				      style={{height: 40, backgroundColor:'#f5f5f5', width:180}}
				      placeholder="Email"
				      onChangeText={(email) => this.setState({email})}
				      value={this.state.email}
				    />

				    <Text style={{padding: 10, fontSize: 18, color:'#000094', marginTop:3}}>
				  	  Contraseña
				  	</Text>
				    <TextInput
				      secureTextEntry={true}
				      style={{height: 40, backgroundColor:'#f5f5f5', width:180}}
				      placeholder="Contraseña"
				      onChangeText={(contraseña) => this.setState({contraseña})}
				      value={this.state.contraseña}
				    />
				  
				  	<Text style={{padding: 10, fontSize: 18, color:'#000094', marginTop:3}}>
				  	  Repetir contraseña
				  	</Text>
				    <TextInput
				      secureTextEntry={true}
				      style={{height: 40, backgroundColor:'#f5f5f5', width:180, marginBottom:15}}
				      placeholder="Contraseña"
				      onChangeText={(contraseñaR) => this.setState({contraseñaR})}
				      value={this.state.contraseñaR}
				    />


				    
					     <Button color='#000094' mode="contained" onPress={() => this._registrar()}>
					      Registrar
					    </Button>
		
				    

				  
				

				</View>
				<Dialog
				   visible={this.state.visible}
				   onDismiss={this._hideDialog}>
				  <Dialog.Title>Alerta</Dialog.Title>
				  <Dialog.Content>
				    <Paragraph>{this.state.mensaje}</Paragraph>
				  </Dialog.Content>
				  <Dialog.Actions>
				    <Button onPress={this._controlarDialog}>Ok</Button>
				  </Dialog.Actions>
				</Dialog>
			</View>
			);


	}



	}

const stilos = StyleSheet.create(
  {


  	header2:{

  	  flex:2,
  	  
  	  justifyContent:'center',
  	  
  	  alignItems:'center',
  	  backgroundColor:'#00009a',

  	}
  }	);
export default Registro;