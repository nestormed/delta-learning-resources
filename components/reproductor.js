/**
 * @flow
 */

import React from "react";
import {
  Dimensions,
  Image,
  Slider,
  StyleSheet,
  Text,
  TouchableHighlight,
  View
} from "react-native";
import { Asset } from "expo-asset";
import { Audio, Video } from "expo-av";
import * as Font from "expo-font";

import { MaterialIcons } from "@expo/vector-icons";

class Icon {
  constructor(module, width, height) {
    this.module = module;
    this.width = width;
    this.height = height;
    Asset.fromModule(this.module).downloadAsync();
  }
}

class PlaylistItem {
  constructor(name, uri, isVideo) {
    this.name = name;
    this.uri = uri;
    this.isVideo = isVideo;
  }
}

let PLAYLIST = global.listaR;

const ICON_THROUGH_EARPIECE = "speaker-phone";
const ICON_THROUGH_SPEAKER = "speaker";

const ICON_PLAY_BUTTON = new Icon(
  require(".././assets/images/delta/png/play.png"),
  22,
  22
);
const ICON_PAUSE_BUTTON = new Icon(
  require(".././assets/images/delta/png/pausa.png"),
  22,
  22
);
const ICON_STOP_BUTTON = new Icon(
  require(".././assets/images/delta/png/stop.png"),
  22,
  22
);
const ICON_FORWARD_BUTTON = new Icon(
  require(".././assets/images/delta/png/forward.png"),
  33,
  25
);
const ICON_BACK_BUTTON = new Icon(
  require(".././assets/images/delta/png/back.png"),
  33,
  25
);

const ICON_LOOP_ALL_BUTTON = new Icon(
  require(".././assets/images/loop_all_button.png"),
  77,
  35
);
const ICON_LOOP_ONE_BUTTON = new Icon(
  require(".././assets/images/loop_one_button.png"),
  77,
  35
);

const ICON_MUTED_BUTTON = new Icon(
  require(".././assets/images/delta/png/muted.png"),
  67,
  58
);
const ICON_UNMUTED_BUTTON = new Icon(
  require(".././assets/images/delta/png/unmuted.png"),
  67,
  58
);

const controlList={control:0};

const ICON_TRACK_1 = new Icon(require(".././assets/images/track_1.png"), 166, 12);
const ICON_THUMB_1 = new Icon(require(".././assets/images/thumb_1.png"), 18, 11);
const ICON_THUMB_2 = new Icon(require(".././assets/images/thumb_2.png"), 15, 11);

const LOOPING_TYPE_ALL = 0;
const LOOPING_TYPE_ONE = 1;
const LOOPING_TYPE_ICONS = { 0: ICON_LOOP_ALL_BUTTON, 1: ICON_LOOP_ONE_BUTTON };

const { width: DEVICE_WIDTH, height: DEVICE_HEIGHT } = Dimensions.get("window");
const BACKGROUND_COLOR = "#00009a";
const BACKGROUND_COLOR2 = "#00009a";
const DISABLED_OPACITY = 1.0;
const FONT_SIZE = 18;
const LOADING_STRING = "... loading ...";
const BUFFERING_STRING = "...buffering...";
const RATE_SCALE = 3.0;
const VIDEO_CONTAINER_HEIGHT = (DEVICE_HEIGHT * 2.0) / 5.0 - FONT_SIZE * 2;

export default class reproductor extends React.Component {
static navigationOptions = {
title: 'Reproductor',
headerStyle: {
backgroundColor: '#00009a',
},
headerTintColor: '#fff',
headerTitleStyle: {
fontWeight: 'bold',
},

};
  constructor(props) {
    super(props);
    this.index = global.indexR;
    this.isSeeking = false;
    this.shouldPlayAtEndOfSeek = false;
    this.playbackInstance = null;
    this.dataSource=[],
    this.state = {
      showVideo: false,
      playbackInstanceName: LOADING_STRING,
      loopingType: LOOPING_TYPE_ALL,
      muted: false,
      playbackInstancePosition: null,
      playbackInstanceDuration: null,
      shouldPlay: false,
      isPlaying: false,
      isBuffering: false,
      isLoading: true,
      fontLoaded: false,
      shouldCorrectPitch: false,
      volume: 1.0,
      rate: 1.0,
      videoWidth: DEVICE_WIDTH,
      videoHeight: VIDEO_CONTAINER_HEIGHT,
      poster: false,
      useNativeControls: false,
      fullscreen: false,
      throughEarpiece: false,
      index:global.indexR-1,
      icon_book:'',
    };
  }

  componentDidMount() {

    Audio.setAudioModeAsync({
      allowsRecordingIOS: false,
      staysActiveInBackground: false,
      interruptionModeIOS: Audio.INTERRUPTION_MODE_IOS_DO_NOT_MIX,
      playsInSilentModeIOS: true,
      shouldDuckAndroid: true,
      interruptionModeAndroid: Audio.INTERRUPTION_MODE_ANDROID_DO_NOT_MIX,
      playThroughEarpieceAndroid: false
    });
    (async () => {
      await Font.loadAsync({
        ...MaterialIcons.font,
        "cutive-mono-regular": require(".././assets/fonts/CutiveMono-Regular.ttf")
      });
      this.setState({ fontLoaded: true });
    })();
    }

  componentWillMount(){
    console.log('oooooooooooooooo============oooooooooooooooo');
    
    PLAYLIST = global.listaR;
    this.index = global.indexR;
    console.log(PLAYLIST);
    console.log('======REEEERERERERRERE===========');
    console.log(global.indexR);
  }
  componentWillMountjhkhkjhhkhk() {

   

    console.log(controlList.control);
    console.log('dede reproductor en willmount');
    console.log(global.listaR);

    if(controlList.control==0){

      controlList.control=1;

     /* console.log('-----------------YYYYYYYYYYYY------------');
      console.log(this.props.navigation.state.params.itemId+' 00000000000000000000');*/
      fetch('https://deltalearningapp.com/get_section_audios/'+this.props.navigation.state.params.itemId)
        .then((response) => response.json())
        .then((responseJson) => {
         
         console.log('TTTTTTTTT_______lllll_____________TTTTTTTTTT');
         console.log(responseJson.audiobooks.length);
          this.setState({
            isLoading: false,
            dataSource: responseJson.audiobooks,
            index: responseJson.audiobook.order,
            icon_book: responseJson.icon_book
            
          }, function(){

            this.borrarInicio();



            this.state.dataSource.forEach(this.armarLista)

           /* console.log(this.state.dataSource);*/
            this.index=this.state.index -1;
            /*console.log(this.index);*/
            source = { uri: PLAYLIST[this.index].uri };
            

          });

        })
        .catch((error) =>{
          console.error(error);
        });
    }



    }


    enviarDataEstadisticas(){
      console.log('*******TTTTTTTTTT***********');
      this.setState({login:true});

      this.setState({ isLoading: true });

      fetch('https://deltalearningapp.com/heard_audio'
        , {
          method: 'POST',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
          },
          body: JSON.stringify({
            email: global.datosUser.email,
            audio_id: global.idAudio,
          }),
        })
      .then((response) => response.json())
      .then((responseJson) => {



      })
      .catch((error) =>{
        console.error(error);

        return
      });


    }

    enviarDataUriEstadisticas(indexx){
      console.log('*******TTTTT55555_____555TTTTT***********');

      fetch('https://deltalearningapp.com/heard_audio_by_url'
        , {
          method: 'POST',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
          },
          body: JSON.stringify({
            email: global.datosUser.email,
            url: PLAYLIST[indexx].uri,
          }),
        })
      .then((response) => response.json())
      .then((responseJson) => {

        console.log('callback uri estadisticas');
        console.log(global.datosUser.email);
        console.log(PLAYLIST[indexx].uri);
        console.log(responseJson.msg);



      })
      .catch((error) =>{
        console.error(error);

        return
      });


    }




  

  async _loadNewPlaybackInstance(playing) {
    console.log('QQQQQQQQ_____________QQQQQQQQQ');
    console.log(global.idAudio);
    console.log(global.datosUser.email);
    this.enviarDataEstadisticas();
    if (this.playbackInstance != null) {
      await this.playbackInstance.unloadAsync();
      // this.playbackInstance.setOnPlaybackStatusUpdate(null);
      this.playbackInstance = null;
    }

    const source = { uri: PLAYLIST[this.index].uri };
    const initialStatus = {
      shouldPlay: playing,
      rate: this.state.rate,
      shouldCorrectPitch: this.state.shouldCorrectPitch,
      volume: this.state.volume,
      isMuted: this.state.muted,
      isLooping: this.state.loopingType === LOOPING_TYPE_ONE
      // // UNCOMMENT THIS TO TEST THE OLD androidImplementation:
      // androidImplementation: 'MediaPlayer',
    };

    if (PLAYLIST[this.index].isVideo) {
      console.log(this._onPlaybackStatusUpdate);
      await this._video.loadAsync(source, initialStatus);
      // this._video.onPlaybackStatusUpdate(this._onPlaybackStatusUpdate);
      this.playbackInstance = this._video;
      const status = await this._video.getStatusAsync();
    } else {
      const { sound, status } = await Audio.Sound.createAsync(
        source,
        initialStatus,
        this._onPlaybackStatusUpdate
      );
      this.playbackInstance = sound;
    }

    this._updateScreenForLoading(false);
  }

  borrarInicio= () =>{
    console.log('en borrar');
    if(PLAYLIST.length==1){

      PLAYLIST.pop();
    }
  }
  armarLista= (item, index) =>{

    /*console.log('funciono');*/
    PLAYLIST.push(
      new PlaylistItem(
        item.title,
        "https://deltalearningapp.com/storage/"+item.stored_name,
        false
      ),);
    /*console.log(PLAYLIST);*/
  }

  _mountVideo = component => {
    console.log('en _mountVideo');
    this._video = component;
    this._loadNewPlaybackInstance(false);
  };

  _updateScreenForLoading(isLoading) {
    if (isLoading) {
      this.setState({
        showVideo: false,
        isPlaying: false,
        playbackInstanceName: LOADING_STRING,
        playbackInstanceDuration: null,
        playbackInstancePosition: null,
        isLoading: true
      });
    } else {
      this.setState({
        playbackInstanceName: PLAYLIST[this.index].name,
        showVideo: PLAYLIST[this.index].isVideo,
        isLoading: false
      });
    }
  }

  _onPlaybackStatusUpdate = status => {
    if (status.isLoaded) {
      this.setState({
        playbackInstancePosition: status.positionMillis,
        playbackInstanceDuration: status.durationMillis,
        shouldPlay: status.shouldPlay,
        isPlaying: status.isPlaying,
        isBuffering: status.isBuffering,
        rate: status.rate,
        muted: status.isMuted,
        volume: status.volume,
        loopingType: status.isLooping ? LOOPING_TYPE_ONE : LOOPING_TYPE_ALL,
        shouldCorrectPitch: status.shouldCorrectPitch
      });
      if (status.didJustFinish && !status.isLooping) {
        this._advanceIndex(true);
        this._updatePlaybackInstanceForIndex(true);
      }
    } else {
      if (status.error) {
        console.log(`FATAL PLAYER ERROR: ${status.error}`);
      }
    }
  };

  _onLoadStart = () => {
    console.log(`ON LOAD START`);
  };

  _onLoad = status => {
    console.log(`ON LOAD : ${JSON.stringify(status)}`);
  };

  _onError = error => {
    console.log(`ON ERROR : ${error}`);
  };

  _onReadyForDisplay = event => {
    const widestHeight =
      (DEVICE_WIDTH * event.naturalSize.height) / event.naturalSize.width;
    if (widestHeight > VIDEO_CONTAINER_HEIGHT) {
      this.setState({
        videoWidth:
          (VIDEO_CONTAINER_HEIGHT * event.naturalSize.width) /
          event.naturalSize.height,
        videoHeight: VIDEO_CONTAINER_HEIGHT
      });
    } else {
      this.setState({
        videoWidth: DEVICE_WIDTH,
        videoHeight:
          (DEVICE_WIDTH * event.naturalSize.height) / event.naturalSize.width
      });
    }
  };

  _onFullscreenUpdate = event => {
    console.log(
      `FULLSCREEN UPDATE : ${JSON.stringify(event.fullscreenUpdate)}`
    );
  };

  _advanceIndex(forward) {
    this.index =
      (this.index + (forward ? 1 : PLAYLIST.length - 1)) % PLAYLIST.length;
  }

  async _updatePlaybackInstanceForIndex(playing) {
    this._updateScreenForLoading(true);

    this.setState({
      videoWidth: DEVICE_WIDTH,
      videoHeight: VIDEO_CONTAINER_HEIGHT
    });

    this._loadNewPlaybackInstance(playing);
  }

  _onPlayPausePressed = () => {
    if (this.playbackInstance != null) {
      if (this.state.isPlaying) {
        this.playbackInstance.pauseAsync();
      } else {
        this.playbackInstance.playAsync();
      }
    }
  };

  _onStopPressed = () => {
    if (this.playbackInstance != null) {
      this.playbackInstance.stopAsync();
    }
  };

  _onForwardPressed = () => {
    if (this.playbackInstance != null) {
      this._advanceIndex(true);
      this._updatePlaybackInstanceForIndex(this.state.shouldPlay);
      this.enviarDataUriEstadisticas(this.index);
      console.log('siguiente audio');
    }
  };

  _onBackPressed = () => {
    if (this.playbackInstance != null) {
      this._advanceIndex(false);
      this._updatePlaybackInstanceForIndex(this.state.shouldPlay);
      this.enviarDataUriEstadisticas(this.index);
      console.log('audio anterior');
    }
  };

  _onMutePressed = () => {
    if (this.playbackInstance != null) {
      this.playbackInstance.setIsMutedAsync(!this.state.muted);
    }
  };

  _onLoopPressed = () => {
    if (this.playbackInstance != null) {
      this.playbackInstance.setIsLoopingAsync(
        this.state.loopingType !== LOOPING_TYPE_ONE
      );
    }
  };

  _onVolumeSliderValueChange = value => {
    if (this.playbackInstance != null) {
      this.playbackInstance.setVolumeAsync(value);
    }
  };

  _trySetRate = async (rate, shouldCorrectPitch) => {
    if (this.playbackInstance != null) {
      try {
        await this.playbackInstance.setRateAsync(rate, shouldCorrectPitch);
      } catch (error) {
        // Rate changing could not be performed, possibly because the client's Android API is too old.
      }
    }
  };

  _onRateSliderSlidingComplete = async value => {
    this._trySetRate(value * RATE_SCALE, this.state.shouldCorrectPitch);
  };

  _onPitchCorrectionPressed = async value => {
    this._trySetRate(this.state.rate, !this.state.shouldCorrectPitch);
  };

  _onSeekSliderValueChange = value => {
    if (this.playbackInstance != null && !this.isSeeking) {
      this.isSeeking = true;
      this.shouldPlayAtEndOfSeek = this.state.shouldPlay;
      this.playbackInstance.pauseAsync();
    }
  };

  _onSeekSliderSlidingComplete = async value => {
    if (this.playbackInstance != null) {
      this.isSeeking = false;
      const seekPosition = value * this.state.playbackInstanceDuration;
      if (this.shouldPlayAtEndOfSeek) {
        this.playbackInstance.playFromPositionAsync(seekPosition);
      } else {
        this.playbackInstance.setPositionAsync(seekPosition);
      }
    }
  };

  _getSeekSliderPosition() {
    if (
      this.playbackInstance != null &&
      this.state.playbackInstancePosition != null &&
      this.state.playbackInstanceDuration != null
    ) {
      return (
        this.state.playbackInstancePosition /
        this.state.playbackInstanceDuration
      );
    }
    return 0;
  }

  _getMMSSFromMillis(millis) {
    const totalSeconds = millis / 1000;
    const seconds = Math.floor(totalSeconds % 60);
    const minutes = Math.floor(totalSeconds / 60);

    const padWithZero = number => {
      const string = number.toString();
      if (number < 10) {
        return "0" + string;
      }
      return string;
    };
    return padWithZero(minutes) + ":" + padWithZero(seconds);
  }

  _getTimestamp() {
    if (
      this.playbackInstance != null &&
      this.state.playbackInstancePosition != null &&
      this.state.playbackInstanceDuration != null
    ) {
      return `${this._getMMSSFromMillis(
        this.state.playbackInstancePosition
      )} / ${this._getMMSSFromMillis(this.state.playbackInstanceDuration)}`;
    }
    return "";
  }

  _onPosterPressed = () => {
    this.setState({ poster: !this.state.poster });
  };

  _onUseNativeControlsPressed = () => {
    this.setState({ useNativeControls: !this.state.useNativeControls });
  };

  _onFullscreenPressed = () => {
    try {
      this._video.presentFullscreenPlayer();
    } catch (error) {
      console.log(error.toString());
    }
  };

  _onSpeakerPressed = () => {
    this.setState(
      state => {
        return { throughEarpiece: !state.throughEarpiece };
      },
      ({ throughEarpiece }) =>
        Audio.setAudioModeAsync({
          allowsRecordingIOS: false,
          interruptionModeIOS: Audio.INTERRUPTION_MODE_IOS_DO_NOT_MIX,
          playsInSilentModeIOS: true,
          shouldDuckAndroid: true,
          interruptionModeAndroid: Audio.INTERRUPTION_MODE_ANDROID_DO_NOT_MIX,
          playThroughEarpieceAndroid: throughEarpiece
        })
    );
  };

  render() {
    return !this.state.fontLoaded ? (
      <View style={styles.emptyContainer} />
    ) : (
      <View style={styles.container}>
        <View />
        <View style={styles.container2}>

          <View style={styles.space} />
            <Text style={[styles.text, { textAlign:'center' }]}>
              {this.state.playbackInstanceName}
            </Text>
          <View style={styles.videoContainer}>

            <Video
              ref={this._mountVideo}
              style={[
                styles.video,
                {
                  opacity: this.state.showVideo ? 1.0 : 0.0,
                  width: this.state.videoWidth,
                  height: 1
                }
              ]}
              resizeMode={Video.RESIZE_MODE_CONTAIN}
              onPlaybackStatusUpdate={this._onPlaybackStatusUpdate}
              onLoadStart={this._onLoadStart}
              onLoad={this._onLoad}
              onError={this._onError}
              onFullscreenUpdate={this._onFullscreenUpdate}
              onReadyForDisplay={this._onReadyForDisplay}
              useNativeControls={this.state.useNativeControls}
            />

            {console.log('ddddddd--'+this.state.icon_book)}

            <Image source={{ uri: 'https://deltalearningapp.com/storage/'+ global.imageLibro }} style={{width: 90*1.5, height: 137.5*1.5}} />
          </View>

        </View>


        <View style={styles.container3}>
          <View
            style={[
              styles.playbackContainer,
              styles.backgroundTos,
              {
                opacity: this.state.isLoading ? DISABLED_OPACITY : 1.0
              }
            ]}
          >
          
            <Slider
              style={styles.playbackSlider}
              trackImage={ICON_TRACK_1.module}
              thumbImage={ICON_THUMB_1.module}
              value={this._getSeekSliderPosition()}
              onValueChange={this._onSeekSliderValueChange}
              onSlidingComplete={this._onSeekSliderSlidingComplete}
              disabled={this.state.isLoading}
              maximumTrackTintColor={'#fff'}
              minimumTrackTintColor={'#fff'}
              thumbTintColor={'#fff'}
            />
            
            <View style={[styles.timestampRow, styles.backgroundDos]}>
              <Text
                style={[
                  styles.text,
                  styles.buffering,
                  styles.colorBlanco,
                  
                ]}
              >
                {this.state.isBuffering ? BUFFERING_STRING : ""}
              </Text>
              <Text
                style={[
                  styles.text,
                  styles.timestamp,
                  styles.colorBlanco
                  
                  
                ]}
              >
                {this._getTimestamp()}
              </Text>
            </View>
          </View>
          <View
            style={[
              styles.buttonsContainerBase,
              styles.buttonsContainerTopRow,
              styles.backgroundDos,
              {
                opacity: this.state.isLoading ? DISABLED_OPACITY : 1.0
              }
            ]}
          >
            <TouchableHighlight
              underlayColor={BACKGROUND_COLOR2}
              style={styles.wrapper}
              onPress={this._onBackPressed}
              disabled={this.state.isLoading}
            >
              <Image style={styles.button} source={ICON_BACK_BUTTON.module} />
            </TouchableHighlight>
            <TouchableHighlight
              underlayColor={BACKGROUND_COLOR2}
              style={styles.wrapper}
              onPress={this._onPlayPausePressed}
              disabled={this.state.isLoading}
            >
              <Image
                style={styles.button}
                source={
                  this.state.isPlaying
                    ? ICON_PAUSE_BUTTON.module
                    : ICON_PLAY_BUTTON.module
                }
              />
            </TouchableHighlight>
            <TouchableHighlight
              underlayColor={BACKGROUND_COLOR2}
              style={styles.wrapper}
              onPress={this._onStopPressed}
              disabled={this.state.isLoading}
            >
              <Image style={styles.button} source={ICON_STOP_BUTTON.module} />
            </TouchableHighlight>
            <TouchableHighlight
              underlayColor={BACKGROUND_COLOR2}
              style={styles.wrapper}
              onPress={this._onForwardPressed}
              disabled={this.state.isLoading}
            >
              <Image style={styles.button} source={ICON_FORWARD_BUTTON.module} />
            </TouchableHighlight>
          </View>
          <View
            style={[
              styles.buttonsContainerBase,
              styles.buttonsContainerMiddleRow
            ]}
          >
            <View style={styles.volumeContainer}>
              <TouchableHighlight
                underlayColor={BACKGROUND_COLOR2}
                style={styles.wrapper}
                onPress={this._onMutePressed}
              >
                <Image
                  style={styles.button}
                  source={
                    this.state.muted
                      ? ICON_MUTED_BUTTON.module
                      : ICON_UNMUTED_BUTTON.module
                  }
                />
              </TouchableHighlight>
              <Slider
                style={styles.volumeSlider}
                trackImage={ICON_TRACK_1.module}
                thumbImage={ICON_THUMB_2.module}
                maximumTrackTintColor={'#fff'}
                minimumTrackTintColor={'#fff'}
                thumbTintColor={'#fff'}
                value={1}
                onValueChange={this._onVolumeSliderValueChange}
              />
            </View>

          </View>
        </View>

        <View />
        {this.state.showVideo ? (
          <View style={{backgroundColor:'#FFF'}}>
            <View
              style={[
                styles.buttonsContainerBase,
                styles.buttonsContainerTextRow
              ]}
            >
              <View />
              <TouchableHighlight
                underlayColor={BACKGROUND_COLOR2}
                style={styles.wrapper}
                onPress={this._onPosterPressed}
              >
                <View style={styles.button}>
                  <Text
                    style={[styles.text]}
                  >
                    Poster: {this.state.poster ? "yes" : "no"}
                  </Text>
                </View>
              </TouchableHighlight>
              <View />
              <TouchableHighlight
                underlayColor={BACKGROUND_COLOR2}
                style={styles.wrapper}
                onPress={this._onFullscreenPressed}
              >
                <View style={styles.button}>
                  <Text
                    style={[styles.text]}
                  >
                    Fullscreen
                  </Text>
                </View>
              </TouchableHighlight>
              <View />
            </View>
            <View style={styles.space,styles.backgroundDos} />
            <View
              style={[
                styles.buttonsContainerBase,
                styles.buttonsContainerTextRow,
                styles.backgroundDos
              ]}
            >
              <View />
              <TouchableHighlight
                underlayColor={BACKGROUND_COLOR2}
                style={styles.wrapper}
                onPress={this._onUseNativeControlsPressed}
              >
                <View style={styles.button}>
                  <Text
                    style={[styles.text]}
                  >
                    Native Controls:{" "}
                    {this.state.useNativeControls ? "yes" : "no"}
                  </Text>
                </View>
              </TouchableHighlight>
              <View />
            </View>
          </View>
        ) : null}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  emptyContainer: {
    alignSelf: "stretch",
    backgroundColor: BACKGROUND_COLOR
  },
  container: {
    flex: 1,
    flexDirection: "column",
    justifyContent: "space-between",
    alignItems: "center",
    alignSelf: "stretch",
    backgroundColor: BACKGROUND_COLOR
  },
  colorBlanco:{
    color:'#fff',
  },
  container2: {
    flex: 5,

    backgroundColor: '#fff'
  },
  container3: {
    flex: 3,
    flexDirection:'column',

    backgroundColor: BACKGROUND_COLOR
  },
  wrapper: {},
  nameContainer: {
    height: FONT_SIZE
  },
  space: {
    height: FONT_SIZE,
   
  },
  videoContainer: {
    height: VIDEO_CONTAINER_HEIGHT,
    width: DEVICE_WIDTH,
    flex:1,
    justifyContent:'center',
    alignItems:'center',
  },
  video: {
    maxWidth: DEVICE_WIDTH
  },
  playbackContainer: {
    flex: 1,
    flexDirection: "column",
    justifyContent: "flex-end",
    alignItems: "center",
    alignSelf: "stretch",

  },
  playbackSlider: {
    alignSelf: "stretch",
    flex:1
  },
  timestampRow: {
    flex: 1,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
    alignSelf: "center",
    minHeight: FONT_SIZE
  },
  text: {
    fontSize: 16,
    minHeight: FONT_SIZE
  },
  buffering: {
    textAlign: "left",
    paddingLeft: 4
  },
  timestamp: {
    textAlign: "right",
    paddingRight: 4
  },
  button: {
    backgroundColor: BACKGROUND_COLOR2,
    width:36,
    height:36
  },
  buttonsContainerBase: {
    flex: 1,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between"
  },
  buttonsContainerTopRow: {
    maxHeight: ICON_PLAY_BUTTON.height,
    minWidth: DEVICE_WIDTH / 1.5,
    maxWidth: DEVICE_WIDTH / 1.5
  },
  buttonsContainerMiddleRow: {
    maxHeight: ICON_MUTED_BUTTON.height,
    alignSelf: "center",
    paddingRight: 20
  },
  volumeContainer: {
    flex: 1,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    alignSelf: "center",
    minWidth: DEVICE_WIDTH / 2.0,
    maxWidth: DEVICE_WIDTH / 2.0
  },
  volumeSlider: {
    width: DEVICE_WIDTH / 2.0 - ICON_MUTED_BUTTON.width
  },
  buttonsContainerBottomRow: {
    maxHeight: ICON_THUMB_1.height,
    alignSelf: "stretch",
    paddingRight: 20,
    paddingLeft: 20
  },
  rateSlider: {
    width: DEVICE_WIDTH / 2.0
  },
  buttonsContainerTextRow: {
    maxHeight: FONT_SIZE,
    alignItems: "center",
    paddingRight: 20,
    paddingLeft: 20,
    minWidth: DEVICE_WIDTH,
    maxWidth: DEVICE_WIDTH
  },
  pressButton:{
    width:42,
    height:42
  },
  backgroundDos:{

    backgroundColor:'#00009a',
  },
  backgroundTos:{

    backgroundColor:'#00009a',
    color:'#000'
  }
});
